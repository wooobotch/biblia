# Biblia

## Amontonamiento de libros

---

### Resúmen y advertencias

El sitio relativo a este repositorio se orienta principalmente la difusión de material de estudio basado en notas personales de una significativa y creciente variedad de temas.

Deben haber suficientes intentos, de tantas maneras como uno quiera imaginar, de aproximar material bibliográfico de apoyo para estudios formales como para no querer pensar en comenzar a probar otra alternativa.

Lo que se alienta a hacer por el presente proyecto es ofrecer alternativas en los modos de estudio y de dictado de clases sin preferencia por una asignatura en particular; lo tratado en los apartados del sitio y en los enlaces recomendados no es orientativo sino introductorio.

Al no tratarse de un producto editorial va a predominar lo colouqial y lo informal, así también la completitud de los servido a la manera de sitio web así como los repositorios y material complementario será presentado como un trabajo en progreso.

### Modo de uso

Leer.

No hay mucha ciencia, algunos de éstos temas no forman parte de ningún curriculum y algunos otros sólo lo hacen muy acotadamente.

Personalmente sólo sugiero incluir ora citas, ora un apartado completo, si es en un contexto de consultas o paralelos a la enseñanza como esté definida en la institución[^1] más próxima a quien estudia.

Mucho debe perderse entre traducciones y síntesis por lo que es de esperarse que tengamos dificultades para tratar individualmente lo que por mi parte presento. Los grupos de estudios y la investigación independiente son lo que verdaderamente nos va a llevar a algún lado.

### Notas posteriores

Espero por los presentes repositorio, sitio web y demás multimedia que asociare eventualmente generar interés en la difusión de conocimiento y en la participación del desarrollo de material libre.

Demasiada formalidad hasta ahora, lo que viene es más relajado, seguramente prefieran quedarse con lo dicho hasta ahora. Si en cambio tienen algo de coraje, sigan leyendo pues.

La educación actual es como tal y no como hace dos siglos o más porque el transporte y las comunicaciones hicieron más fácil su adopción y difusión. Np por eso es la mejor opción ni la única.

En la época de mis abuelos y bisabuelos la educación comenzaba en casa y continuaba de por vida en casa, se leía (si se sabía cómo), se enseñaban procesos y prácticas (en algunos casos para oficios, en otros para hacer la vida doméstica más llevadera); existían cursos por correo.

Nada nuevo bajo el sol: se estudia por internet, con variedad de dispositivos y herramientas, gratuitamente o por suscripción paga.

Esa parte que siempre estuvo presente: desde las cavernas hasta el pod recomendado por el WEF, la humanidad se esforzó por dar forma a las ideas propias y ayudar a otros en ese mismo proceso.

No es conmigo que van a recuperar el timón de la educación sino ustedes mismos, independientemente de mis actividades.

### Educación

A lo largo de todo este escrito utilicé la palabra educación únicamente, sin distinguirla de instrucción. Las instituciones formales, como universidades, no pueden ser reemplazadas; sus objetivos para con la sociedad y el individuo están bien definidos.

Seguramente cuando opten por uno de los temas para comenzar su estudio tendrán objetivos propios y, por la naturaleza de mi *oferta académica*, también se fijarán una meta no necesariamente igual a la que podrían tomar por un *curso de instrucción*: tomen cuanto haga falta sin ningún tipo de apuro, que les sirva su selección para ser mejores personas o para estar más a gusto consigo mismos.


[^1]:Por *institución* se interpreta academia, escuela, etc. Por *próxima*, de estudios en curso.
